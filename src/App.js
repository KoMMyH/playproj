import React from 'react';
import './App.css';

const FIELD_SIZE = 30;

const GAME_SPEED_TIMEOUT = 100;

class App extends React.Component {
  state = {
    field: Array.from({length: FIELD_SIZE}),
    bird: {x: 2, y: 5},
    isGameStarted: false,
    pillar: {x: 10, y: 6},
    isClicked: false,
    gameOver: false,
    timeoutId: 0
  };

  isBird = ({x, y}) => {
    const {bird} = this.state;
    return x === bird.x && y === bird.y;
  };

  isPillar = ({x, y}) => {
    const { pillar } = this.state;
    return x === pillar.x && (y - 3 >= pillar.y || y + 3 <= pillar.y);
  };

  generatePillar = () => {
    const pillarX = Math.floor(Math.random() * 20) + 9;
    const pillarY = Math.floor(Math.random() * 6) + 5;
    return {
      x: pillarX,
      y: pillarY
    };
  };

  startGame = () => {
    const pillar = this.generatePillar();
    this.setState({
      isGameStarted: true,
      gameOver: false,
      isClicked: false,
      bird: { x: 2, y: 5 },
      pillar: pillar
    });
    this.gameCycle();
  };

  moveField = () => {
    const { pillar } = this.state;
    if (!pillar || pillar.x - 1 < 0)
      return this.generatePillar();
    else
      return {
        x: pillar.x - 1,
        y: pillar.y
      };
  };

  findCollision = () => {
    const {pillar, bird} = this.state;
    if (pillar.x === bird.x && (bird.y - 3 >= pillar.y || bird.y + 3 <= pillar.y)) {
      this.setState({
        gameOver: true,
        isGameStarted: false
      });
    }
  };

  moveBird = () => {
    const {bird, isClicked} = this.state;
    if (isClicked) {
      return {
        x: bird.x,
        y: bird.y - 2
      }
    } else {
      return {
        x: bird.x,
        y: bird.y + 1
      }
    }
  };

  componentDidUpdate() {
    if (this.state.gameOver) {
      clearTimeout(this.state.timeoutId);
    }
  }

  gameCycle = () => {
    const id = setTimeout(this.gameCycle, GAME_SPEED_TIMEOUT);
    const newPillar = this.moveField();
    const newBirdPos = this.moveBird();
    this.findCollision();
    this.setState({
      pillar: newPillar,
      bird: newBirdPos,
      isClicked: false,
      timeoutId: id
    });
  };

  componentWillUnmount() {
    const {timeoutId} = this.state;
    clearTimeout(timeoutId);
  }

  setFieldClick = () => this.setState({isClicked: true})

  render() {
    const {field, gameOver} = this.state;

    return (
      <div>
        <div className="Field" onClick={this.setFieldClick}>
          {
            field.map((item1, indexY) => {
              return (
                <div className="Row">
                  {
                    field.map((item2, indexX) => (
                      <div className={`Box 
                    ${this.isBird({x: indexX, y: indexY}) ? 'Bird' : ''} 
                    ${this.isPillar({x: indexX, y: indexY}) ? 'Pillar' : ''}
                    `}/>
                    ))
                  }
                </div>
              )
            })
          }
        </div>
        <button onClick={this.startGame}>
          Start game
        </button>
        {
          gameOver &&
          <div>
            YOU LOSE!
          </div>
        }
      </div>
    )
  }
}

export default App;
